﻿using UnityEngine;
using System.Collections;

public class csCamera2D : MonoBehaviour {
	[SerializeField]
	private float xMax = 0;
	[SerializeField]
	private float yMax = 0;
	[SerializeField]
	private float xMin = 0;
	[SerializeField]
	private float yMin = 0;

	private Transform target;

	// Use this for initialization
	void Start () {
		target = GameObject.Find ("Player").transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.position = new Vector3 (Mathf.Clamp(target.position.x, xMin, xMax), Mathf.Clamp(target.position.y, yMin, yMax), transform.position.z);

	}
}
