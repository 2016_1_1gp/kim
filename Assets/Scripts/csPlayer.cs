﻿using UnityEngine;
using System.Collections;

public class csPlayer : MonoBehaviour {
	public int pHP = 5;
	public int pDamage = 1;

	Animator playerAnimetion;

	GameObject aPsword;
	Animator aniPsword;

    // Use this for initialization
    void Start () {
		playerAnimetion = GetComponent<Animator>();

		aPsword = GameObject.Find("Sword");
		aniPsword = aPsword.GetComponent<Animator>();
	}

	// Update is called once per frame
	void FixedUpdate () {
        //공격
		PAtt();
    }

	void OnTriggerEnter2D(Collider2D coll){
		if (this.gameObject.tag == "g_Player" && coll.gameObject.tag == "g_Enemy") {
			//pHP -= 1;
		}
	}

	void OnGUI(){
		GUI.Label (Rect.MinMaxRect (10, 10, 120, 120), "HP : " + pHP);
	}

	void PAtt(){
		if (Input.GetKey(KeyCode.Z) && playerAnimetion.GetFloat("Speed") == 0)
		{
			playerAnimetion.SetBool("att", true);
			aniPsword.SetBool("att", true);

		}
		else {
			playerAnimetion.SetBool("att", false);
			aniPsword.SetBool("att", false);
		}
	}
}
