﻿using UnityEngine;
using System.Collections;

public class csShaft : MonoBehaviour {

	bool isRight = true;

	public int fMoveSpeed = 1;

	public GameObject EnemyArch;

	// Use this for initialization
	void Awake () {

	}

	// Update is called once per frame
	void Update () {
			transform.Translate (Vector2.left * fMoveSpeed * Time.deltaTime);
	}

	void OnTriggerEnter2D(Collider2D coll){
		if(coll.gameObject.tag == "g_Player")
			Destroy (gameObject, 0.1f);
	}
}
