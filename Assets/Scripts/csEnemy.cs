﻿using UnityEngine;
using System.Collections;

public class csEnemy : MonoBehaviour {
	bool isFollow = false;
	bool pIsRight = true;
	bool isAtt = false;
	public static int eHP = 3;
	float fMoveSpeed = 1.0f;
	float attRate = 1.0f;
	float attNext = 0.0f;

	public GameObject PlayerChar;
	public GameObject PlayerChar2;

	Animator EAnimation;

	// Use this for initialization
	void Start () {
		EAnimation = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (isFollow) {
			if (!EAnimation.GetBool ("Run"))
				EAnimation.SetBool ("Run", true);
			EMove ();
		} else if (isAtt && Time.time > attNext) {
			attNext = Time.time + attRate;
			EAtt ();
			if (!EAnimation.GetBool ("Att"))
				EAnimation.SetBool ("Att", true);
			if (EAnimation.GetBool ("Run"))
				EAnimation.SetBool ("Run", false);
		} else {
			EAnimation.SetBool ("Run", false);
			EAnimation.SetBool ("Att", false);
		}
	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "g_Player" && Mathf.Abs (PlayerChar.transform.position.x - transform.position.x) > 0.6f) {
			isFollow = true;
			isAtt = false;
		}
	}

	void OnTriggerStay2D(Collider2D coll){
		if(coll.gameObject.tag == "g_Player" && Mathf.Abs (PlayerChar.transform.position.x - transform.position.x) <= 0.6f){
			isFollow = false;
			isAtt = true;
		}
	}

	void OnTriggerExit2D(Collider2D coll){
		if (coll.gameObject.tag == "g_Player") {
			isFollow = false;
			isAtt = false;
		}
	}

	void EMove(){
		float toMove = fMoveSpeed * Time.deltaTime;

		if (PlayerChar.transform.position.x > transform.position.x) {
			transform.Translate (Vector2.right * toMove);
			if(!pIsRight)
				PTurnDir ();
		} else{
			transform.Translate (Vector2.left * toMove);
			if(pIsRight)
				PTurnDir ();
		}
	}
	void PTurnDir(){
		pIsRight = !pIsRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}

	void EAtt(){
		PlayerChar2.GetComponent<csPlayer> ().pHP -= 1;
	}
}
