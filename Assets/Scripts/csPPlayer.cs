﻿using UnityEngine;
using System.Collections;

public class csPPlayer : MonoBehaviour {
	public float playerSpeed = 5;
	bool pIsRight = true;

	public float playerJumpF = 400.0f;
	public Rigidbody2D PlayerRigid;

	public Transform pTrans;
	Vector3 afterPosition;

	//점프 충돌체크용
	bool playerGrounded = false;
	public Transform groundedEnd;

	GameObject aPsword;
	Animator aniPsword;
	GameObject aPlayer;
	Animator playerAnimetion;

	// Use this for initialization
	void Start () {
		aPlayer = GameObject.Find("Player");
		playerAnimetion = aPlayer.GetComponent<Animator>();

		aPsword = GameObject.Find("Sword");
		aniPsword = aPsword.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//좌우 이동
		PRun();

		//점프
		PJump();
	}


	void PRun(){
		float playerMove = Input.GetAxis ("Horizontal");

		if (!playerAnimetion.GetBool ("att")){ // 공격중 이동 불가
			if (Global.isPAtt == false) {
				playerAnimetion.SetFloat ("Speed", Mathf.Abs (playerMove)); // 이동 애니메이션
				aniPsword.SetFloat ("Run", Mathf.Abs (playerMove));
				GetComponent<Rigidbody2D> ().velocity = new Vector2 (playerMove * playerSpeed, GetComponent<Rigidbody2D> ().velocity.y);
			}
		}
		//좌우 반전
		if (playerMove > 0 && !pIsRight)
			PTurnDir ();
		else {
			if (playerMove < 0 && pIsRight)
				PTurnDir ();
		}
	}
	void PTurnDir(){
		pIsRight = !pIsRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}

	void PJump(){
		playerGrounded = Physics2D.Linecast(this.transform.position, groundedEnd.position, 1 << LayerMask.NameToLayer("Ground"));

		if (Input.GetKeyDown (KeyCode.Space) && playerGrounded == true) {
			PlayerRigid.AddForce (Vector2.up * playerJumpF);
			playerAnimetion.SetBool("Jumpu", true);
			aniPsword.SetBool ("Jump", true);
		}
		if (afterPosition.y > pTrans.position.y) {
			playerAnimetion.SetBool ("Jumpd", true);
			playerAnimetion.SetBool ("Jumpu", false);
		}
		if (playerGrounded == true) {
			if(aniPsword.GetBool("Jump") == true && playerAnimetion.GetBool("Jumpd") == true)
				aniPsword.SetBool ("Jump", false);
			playerAnimetion.SetBool ("Jumpd", false);
		}
		afterPosition = pTrans.position;
	}

}
